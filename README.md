Restminator es un proyecto para montar de manera rápida un servidor REST con Jersey, Guice y Grizzly. Permite hacerlo sin apenas configuración, como aplicación Java standalone. Es un buen punto de partida para publicar de manera rápida y ligera servicios REST, sin pesados servidores ni configuraciones tediosas. Además, esta orientado a servidores cloud, pues provee de una sonda inicial para revisar el estado del servicio.

Sobre la base de este servidor de ha montado el API de Atresmedia Conecta:
http://kcy.me/116an
http://kcy.me/19xs4
http://kcy.me/19xs7