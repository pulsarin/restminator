package com.restminator;

public class IllegalConfigException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;
	
	public IllegalConfigException(String message){
		super(new StringBuilder("")
		.append("                                                                      \n")
		.append("              ._,.                                                    \n")
		.append("         \"..-..pf.                                                    \n")
		.append("        -L   ..#'                                                     \n")
		.append("      .+_L  .\"]#                                                      \n")
		.append("      ,'j' .+.j`                 -'.__..,.,p.                         \n")
		.append("     _~ #..<..0.                 .J-.``..._f.                         \n")
		.append("    .7..#_.. _f.                .....-..,`4'                          \n")
		.append("    ;` ,#j.  T'      ..         ..J....,'.j`                          \n")
		.append("   .` ..\"^.,-0.,,,,yMMMMM,.    ,-.J...+`.j@                           \n")
		.append("  .'.`...' .yMMMMM0M@^=`\"\"g.. .'..J..\".'.jH                           \n")
		.append("  j' .'1`  q'^)@@#\"^\".`\"='BNg_...,]_)'...0-                           \n")
		.append(" .T ...I. j\"    .'..+,_.'3#MMM0MggCBf....F.                           \n")
		.append(" j/.+'.{..+       `^~'-^~~\"\"\"\"'\"\"\"?'\"``'1`                            \n")
		.append(" .... .y.}                  `.._-:`_...jf                             \n")
		.append(" g-.  .Lg'                 ..,..'-....,'.                             \n")
		.append(".'.   .Y^                  .....',].._f                               \n")
		.append("......-f.                 .-,,.,.-:--&`                               \n")
		.append("                          .`...'..`_J`                                \n")
		.append("                          .~......'#'                                 \n")
		.append("                          '..,,.,_]`                                  \n")
		.append("                          .L..`..``.                                  \n")
		.append("                                                                      \n")
		.append("                                                                      \n")
		.append(message)
		.append("                                                                      \n").toString());

	}
	
	


}
