package com.restminator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.name.Names;
import com.restminator.domain.RestminatorParams;
import com.restminator.module.ControllersModule;
import com.restminator.module.PropertiesModule;
import com.restminator.server.Restminator;

public class Main {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Injector injector = Guice.createInjector(new PropertiesModule("/server.properties"));
		String port = injector.getInstance(Key.get(String.class,
				Names.named("server.port")));
		/* Modules for GUICE*/
		List<Module> modules = new ArrayList<Module>();
		/* Default controllers */
		modules.add(new ControllersModule());
		/* Properties module */
		modules.add(new PropertiesModule("server.properties"));
		Restminator.getInstance().configure(
				new RestminatorParams.Builder()
						.setInjector(Guice.createInjector(modules))
						.setPort(Integer.parseInt(port))
						.build())
				.start();
	}
}