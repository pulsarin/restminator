package com.restminator.controllers;

import java.lang.management.ManagementFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.restminator.domain.SystemInfo;
import com.restminator.domain.TimerHelper;

@Path("/admin/")
public class ServerController {

	@GET
	@Path("status")
	@Produces({MediaType.APPLICATION_JSON , MediaType.TEXT_XML})
	public SystemInfo getStatus() {
		TimerHelper timer = new TimerHelper();
		SystemInfo info = new SystemInfo.Builder()
				.setCpuLoadAverage(ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage())
				.setFreeMemory(Runtime.getRuntime().freeMemory())
				.setMaxMemory(Runtime.getRuntime().maxMemory())
				.setNumberOfProcessors(Runtime.getRuntime().availableProcessors())
				.setTotalMemory(Runtime.getRuntime().totalMemory())
				.build();
		info.setElapsedTime(timer.getElapsedTime());
		return info;
	}

}