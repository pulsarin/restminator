package com.restminator.controllers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.restminator.server.Restminator;

@Path("/config/")
public class ThreadPoolController {

	@GET
	@Path("pool/{name}/{corePoolSize}/{maxPoolSize}/{queueLimit}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String configure(@PathParam("name") String name, @PathParam("corePoolSize") int corePoolSize, @PathParam("maxPoolSize") int maxPoolSize,
			@PathParam("queueLimit") int queueLimit) {
		Restminator.configureThreadPool(name, corePoolSize, maxPoolSize, queueLimit);
		return "ok";
	}
}