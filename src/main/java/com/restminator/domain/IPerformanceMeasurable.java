package com.restminator.domain;


public interface IPerformanceMeasurable {
	
	Long getElapsedTime();
}
