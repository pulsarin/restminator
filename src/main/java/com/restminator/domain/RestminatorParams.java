package com.restminator.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.inject.Injector;

/**
 * POJO for Server init params
 * @author sgg
 */
public class RestminatorParams implements Serializable {

	private static final long serialVersionUID = -6906181407451785406L;
	private int port;
	private Injector injector;
	private String poolName;
	private Integer corePoolSize;
	private Integer maxPoolSize;
	private Integer queueLimit;
	private Map<String, String> initParams = new HashMap<String, String>();
	
	public static class Builder{
		private int port = 80;
		private String poolName;
		private Integer corePoolSize;
		private Integer maxPoolSize;
		private Integer queueLimit;
		private Injector injector;
		private Map<String, String> initParams = new HashMap<String, String>();

		/**
		 * @return the port
		 */
		public int getPort() {
			return port;
		}

		/**
		 * @param port the port to set
		 */
		public Builder setPort(int port) {
			this.port = port;
			return this;
		}

		public RestminatorParams build() {
			return new RestminatorParams(this);
		}

		/**
		 * @return the corePoolSize
		 */
		public Integer getCorePoolSize() {
			return corePoolSize;
		}

		/**
		 * @param corePoolSize the corePoolSize to set
		 * @return
		 */
		public Builder setCorePoolSize(int corePoolSize) {
			this.corePoolSize = corePoolSize;
			return this;
		}

		/**
		 * @return the maxPoolSize
		 */
		public Integer getMaxPoolSize() {
			return maxPoolSize;
		}

		/**
		 * @param maxPoolSize
		 * @return
		 */
		public Builder setMaxPoolSize(int maxPoolSize) {
			this.maxPoolSize = maxPoolSize;
			return this;
		}

		/**
		 * @return the poolName
		 */
		public String getPoolName() {
			return poolName;
		}

		/**
		 * @param poolName the poolName to set
		 */
		public Builder setPoolName(String poolName) {
			this.poolName = poolName;
			return this;
		}

		/**
		 * @return the queueLimit
		 */
		public Integer getQueueLimit() {
			return queueLimit;
		}

		/**
		 * @param queueLimit the queueLimit to set
		 */
		public Builder setQueueLimit(Integer queueLimit) {
			this.queueLimit = queueLimit;
			return this;
		}

		/**
		 * @return the injector
		 */
		public Injector getInjector() {
			return injector;
		}

		/**
		 * @param injector the injector to set
		 */
		public Builder setInjector(Injector injector) {
			this.injector = injector;
			return this;
		}

		/**
		 * @return the initParams
		 */
		public Map<String, String> getInitParams() {
			return initParams;
		}

		/**
		 * @param initParams the initParams to set
		 */
		public Builder setInitParams(Map<String, String> initParams) {
			this.initParams = initParams;
			return this;
		}
	}

	/**
	 * Creates a RestminatorParams
	 * @param builder RestminatorParamsBuilder thats contains init params
	 */
	public RestminatorParams(Builder builder) {
		this.port = builder.getPort();
		this.poolName = builder.getPoolName();
		this.corePoolSize = builder.getCorePoolSize();
		this.maxPoolSize = builder.getMaxPoolSize();
		this.queueLimit = builder.getQueueLimit();
		this.injector = builder.getInjector();
		this.initParams = builder.getInitParams();
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the corePoolSize
	 */
	public Integer getCorePoolSize() {
		return corePoolSize;
	}

	/**
	 * @return the maxPoolSize
	 */
	public Integer getMaxPoolSize() {
		return maxPoolSize;
	}

	/**
	 * @param corePoolSize the corePoolSize to set
	 */
	public void setCorePoolSize(Integer corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	/**
	 * @param maxPoolSize the maxPoolSize to set
	 */
	public void setMaxPoolSize(Integer maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	/**
	 * @return the poolName
	 */
	public String getPoolName() {
		return poolName;
	}

	/**
	 * @param poolName the poolName to set
	 */
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	/**
	 * @return the queueLimit
	 */
	public Integer getQueueLimit() {
		return queueLimit;
	}

	/**
	 * @param queueLimit the queueLimit to set
	 */
	public void setQueueLimit(Integer queueLimit) {
		this.queueLimit = queueLimit;
	}

	/**
	 * @return the injector
	 */
	public Injector getInjector() {
		return injector;
	}

	/**
	 * @param injector the injector to set
	 */
	public void setInjector(Injector injector) {
		this.injector = injector;
	}

	/**
	 * @return the initParams
	 */
	public Map<String, String> getInitParams() {
		return initParams;
	}

	/**
	 * @param initParams the initParams to set
	 */
	public void setInitParams(Map<String, String> initParams) {
		this.initParams = initParams;
	}
}
