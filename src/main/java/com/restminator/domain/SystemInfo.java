package com.restminator.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "systeminfo")
public class SystemInfo implements Serializable, IPerformanceMeasurable{
	private static final long serialVersionUID = -7000888373859730649L;
	private Double cpuLoadAverage;
	private long freeMemory;
	private long maxMemory;
	private int numberOfProcessors;
	private long totalMemory;
	private long elapsedTime = -1;
	
	public static class Builder{
		private Double cpuLoadAverage;
		private long freeMemory;
		private long maxMemory;
		private int numberOfProcessors;
		private long totalMemory;
		private long elapsedTime;

		public Double getCpuLoadAverage() {
			return cpuLoadAverage;
		}

		public Builder setCpuLoadAverage(Double cpuLoadAverage) {
			this.cpuLoadAverage = cpuLoadAverage;
			return this;
		}

		public long getFreeMemory() {
			return freeMemory;
		}

		public Builder setFreeMemory(long freeMemory) {
			this.freeMemory = freeMemory;
			return this;
		}

		public long getMaxMemory() {
			return maxMemory;
		}

		public Builder setMaxMemory(long maxMemory) {
			this.maxMemory = maxMemory;
			return this;
		}

		public int getNumberOfProcessors() {
			return numberOfProcessors;
		}

		public Builder setNumberOfProcessors(int numberOfProcessors) {
			this.numberOfProcessors = numberOfProcessors;
			return this;
		}

		public long getTotalMemory() {
			return totalMemory;
		}

		public Builder setTotalMemory(long totalrMemory) {
			this.totalMemory = totalrMemory;
			return this;
		}

		public long getElapsedTime() {
			return elapsedTime;
		}

		public Builder setResponseTime(long elapsedTime) {
			this.elapsedTime = elapsedTime;
			return this;
		}

		public SystemInfo build() {
			return new SystemInfo(this);
		}
	}
	
	public SystemInfo(){}
	
	public SystemInfo(Builder builder){
		this.cpuLoadAverage = builder.getCpuLoadAverage();
		this.freeMemory = builder.getFreeMemory();
		this.maxMemory = builder.getMaxMemory();
		this.numberOfProcessors = builder.getNumberOfProcessors();
		this.totalMemory = builder.getTotalMemory();
		this.elapsedTime = builder.getElapsedTime();
	}
	
	public Double getCpuLoadAverage() {
		return cpuLoadAverage;
	}
	public void setCpuLoadAverage(Double cpuLoadAverage) {
		this.cpuLoadAverage = cpuLoadAverage;
	}
	public long getFreeMemory() {
		return freeMemory;
	}
	public void setFreeMemory(long freeMemory) {
		this.freeMemory = freeMemory;
	}
	public long getMaxMemory() {
		return maxMemory;
	}
	public void setMaxMemory(long maxMemory) {
		this.maxMemory = maxMemory;
	}
	public int getNumberOfProcessors() {
		return numberOfProcessors;
	}
	public void setNumberOfProcessors(int numberOfProcessors) {
		this.numberOfProcessors = numberOfProcessors;
	}
	public long getTotalMemory() {
		return totalMemory;
	}
	public void setTotalMemory(long totalMemory) {
		this.totalMemory = totalMemory;
	}

	@Override
	public Long getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
}
