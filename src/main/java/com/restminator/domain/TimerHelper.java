/**
 * 
 */
package com.restminator.domain;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * Timer helper for performante stats
 * @author sgg74018
 *
 */
public class TimerHelper implements Serializable,IPerformanceMeasurable{
	
	private static final long serialVersionUID = 1L;
	private final Long start;
	
	/**
	 * Creates a TimerHelper and starts it
	 */
	public TimerHelper(){
		start = new DateTime().getMillis();
	}
	
	/**
	 * The elapsed time since timer's start
	 * @return the elapsed time in millis
	 */
	@Override
	public Long getElapsedTime(){
		return new DateTime().getMillis() - start;
	}
}
