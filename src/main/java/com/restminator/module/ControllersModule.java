/**
 * 
 */
package com.restminator.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.servlet.ServletModule;
import com.restminator.controllers.ServerController;
import com.restminator.controllers.ThreadPoolController;
import com.restminator.server.CacheFilterFactory;

/**
 * @author sgg74018
 */
public class ControllersModule extends ServletModule {

	private static final Logger LOG = LoggerFactory.getLogger(ControllersModule.class);

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.servlet.ServletModule#configureServlets()
	 */
	@Override
	protected void configureServlets() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Creating bindings for Guice");
		}
		bind(ServerController.class);
		bind(CacheFilterFactory.class);
		bind(ThreadPoolController.class);
	}

}
