/**
 * 
 */
package com.restminator.module;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * @author sgg
 */
public class PropertiesModule extends AbstractModule {

	private static final Logger LOG = LoggerFactory.getLogger(PropertiesModule.class);

	private final Properties properties;

	public PropertiesModule(final String propertiesFile) {
		properties = new Properties();
		try {
			properties.load(this.getClass().getResourceAsStream(propertiesFile));
		} catch (final Exception e) {
			LOG.error(new StringBuilder("Error loading properties file").append(propertiesFile).append(". Stopping...").toString(), e);
			System.exit(1);
		}
	}

	public PropertiesModule(final List<String> propertiesFiles) {
		properties = new Properties();
		try {
			for (final String propertiesFile : propertiesFiles) {
				properties.load(this.getClass().getResourceAsStream(propertiesFile));
			}
		} catch (final Exception e) {
			LOG.error(new StringBuilder("Error loading properties file ").append(propertiesFiles.toString()).append(". Stopping...")
					.toString(), e);
			System.exit(1);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		Names.bindProperties(binder(), properties);
	}

}
