package com.restminator.server;

import java.util.Collections;
import java.util.List;

import com.google.inject.Singleton;
import com.restminator.domain.CacheControlHeader;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

@Singleton
public class CacheFilterFactory implements ResourceFilterFactory {
	private static final List<ResourceFilter> NO_CACHE_FILTER = Collections.<ResourceFilter> singletonList(new CacheResponseFilter(
			"max-age=0,no-cache,no-store,private"));

	@Override
	public List<ResourceFilter> create(AbstractMethod am) {
		CacheControlHeader cch = am.getAnnotation(CacheControlHeader.class);
		if (cch == null) {
			return NO_CACHE_FILTER;
		} else {
			return Collections.<ResourceFilter> singletonList(new CacheResponseFilter(cch.value()));
		}
	}

}