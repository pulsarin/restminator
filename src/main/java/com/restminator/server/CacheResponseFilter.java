package com.restminator.server;

import javax.ws.rs.core.HttpHeaders;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

public class CacheResponseFilter implements ResourceFilter, ContainerResponseFilter {

	private final String headerValue;

	CacheResponseFilter(String headerValue) {
		this.headerValue = headerValue;
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return null;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		return this;
	}

	@Override
	public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
		// attache Cache Control header to each response based on the annotation value
		response.getHttpHeaders().putSingle(HttpHeaders.CACHE_CONTROL, headerValue);
		return response;
	}
}