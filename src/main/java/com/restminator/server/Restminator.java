package com.restminator.server;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.servlet.FilterRegistration;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.grizzly.threadpool.GrizzlyExecutorService;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.servlet.GuiceFilter;
import com.restminator.IllegalConfigException;
import com.restminator.domain.IPerformanceMeasurable;
import com.restminator.domain.RestminatorParams;
import com.restminator.domain.TimerHelper;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class Restminator extends Thread implements IPerformanceMeasurable {

	private static final Logger LOG = LoggerFactory.getLogger(Restminator.class);

	private boolean markedForInterruption = Boolean.FALSE;
	protected static Thread thread;
	private static Restminator instance = null;
	private static HttpServer server;
	public static URI BASE_URI;
	private RestminatorParams params;
	private boolean configured = Boolean.FALSE;
	private final TimerHelper timer;

	public Restminator() {
		super("REST server");
		timer = new TimerHelper();
	}

	@Override
	public synchronized void start() {
		this.markedForInterruption = Boolean.FALSE;
		super.start();
	}

	public synchronized Restminator configure(RestminatorParams params) {
		this.params = params;
		this.configured = true;
		return instance;
	}

	public static Restminator getInstance() {
		if (instance == null) {
			instance = new Restminator();
		}
		return instance;
	}

	private static void doStart(RestminatorParams params) throws IllegalArgumentException, NullPointerException, IOException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Starting grizzly on port ".concat(String.valueOf(params.getPort())));
		}
		server = HttpServer.createSimpleServer("/", params.getPort());
		if (params.getInjector() != null) {
			WebappContext context = new WebappContext("Restminator", "");
			GuiceFilter f = new GuiceFilter();
			FilterRegistration reg = context.addFilter("guiceFilter", f);
			reg.addMappingForUrlPatterns(null, "/*");
			ServletRegistration sreg = context.addServlet("guiceContainer", new GuiceContainer(params.getInjector()));
			sreg.addMapping("/*");
			sreg.setInitParameters(params.getInitParams());
			context.deploy(server);
		}
		server.start();
		if (params.getCorePoolSize() != null) {
			configureThreadPool(params.getPoolName(), params.getCorePoolSize(), params.getMaxPoolSize(), params.getQueueLimit());
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("Server started in " + instance.getElapsedTime());
		}
	}

	public static void configureThreadPool(String name, int corePoolSize, int maxPoolSize, int queueLimit) {
		ThreadPoolConfig config = ThreadPoolConfig.defaultConfig().
				setPoolName(name).
				setCorePoolSize(corePoolSize).
				setMaxPoolSize(maxPoolSize)
				.setQueueLimit(queueLimit);
		// reconfigure the thread pool
		NetworkListener listener = server.getListeners().iterator().next();
		GrizzlyExecutorService threadPool = (GrizzlyExecutorService) listener.getTransport().getWorkerThreadPool();
		threadPool.reconfigure(config);
	}

	@Override
	public void run() {
		super.run();
		if (configured) {
			try {
				doStart(params);
			} catch (IllegalArgumentException e1) {
				LOG.error("Illegal argument received for start server.", e1);
			} catch (NullPointerException e1) {
				LOG.error("Illegal argument received for start server.", e1);
			} catch (IOException e1) {
				LOG.error("Illegal argument received for start server.", e1);
			}
			while (!this.markedForInterruption) {
				try {
					super.sleep(2000);
				} catch (InterruptedException e) {
					LOG.error("Caught an exception while sleeping. Description: " + e.getMessage());
				}
			}
		} else {
			LOG.error("Server is NOT configured! Stopping...");
			throw new IllegalConfigException("Server is NOT configured! Stopping...");
		}
	}

	@Override
	public State getState() {
		return super.getState();
	}

	public void startServer() {
		if (Restminator.server != null) {
			try {
				Restminator.server.start();
			} catch (IOException e) {
				LOG.error("The server cannot start.", e);
			}
		}
	}

	public void stopServer() {
		server.stop();
		shutdown();
	}

	public void shutdown() {
		this.markedForInterruption = Boolean.TRUE;
	}

	public static boolean isConfigured() {
		return instance.configured;
	}

	@Override
	public Long getElapsedTime() {
		return timer.getElapsedTime();
	}
}
