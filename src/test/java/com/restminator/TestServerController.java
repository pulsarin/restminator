/**
 * 
 */
package com.restminator;

import java.io.IOException;

import junit.framework.TestCase;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import com.restminator.domain.SystemInfo;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;

/**
 * @author sgg74018
 */
public class TestServerController extends JerseyTest {
	public TestServerController() throws Exception {
		super("com.restminator.controllers");
	}

	@Test
	public void testServerStatus() throws IOException {
		WebResource webResource = resource();
		String responseMsg = webResource.path("admin/status").get(String.class);
		SystemInfo info = new ObjectMapper().readValue(responseMsg, SystemInfo.class);
		TestCase.assertNotNull(info);
	}

}
